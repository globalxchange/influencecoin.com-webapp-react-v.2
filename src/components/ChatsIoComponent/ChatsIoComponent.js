import React, { useContext, useState } from "react";
// import ChatsIoComponent from "@teamforce/chats.io-plugin";
// import "@teamforce/chats.io-plugin/dist/index.css";

import logo from "../../static/images/logos/influenceCoin.svg";
import { ChatsContext } from "../../context/GlobalContext";

function ChatsIoComponentM() {
  const { setChatOn } = useContext(ChatsContext);
  // eslint-disable-next-line no-unused-vars
  const [tabSelected, setTabSelected] = useState();
  return (
    <ChatsIoComponent
      tabOpen={tabSelected}
      primaryColor="#464b4e"
      secondaryColor="#e7e7e7"
      appCode="ice"
      logo={logo}
      onClose={() => setChatOn(false)}
      right
    />
  );
}

// export default ChatsIoComponentM;
