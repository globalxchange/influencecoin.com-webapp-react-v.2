import React, { useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Axios from "axios";

function CoinSelectModal({ coinList, setCoin, onClose }) {
  const [searchStr, setSearchStr] = useState([]);
  //sdsd
  return (
    <>
      <div className="modalCountrySelect">
        <div className="overlayClose" role="presentation" onClick={() => onClose()} onKeyDown={() => onClose()}/>
        <div className="modalContent">
          <div className="head">Change Display Currency</div>
          <div className="content">
            <input
              value={searchStr}
              type="text"
              placeholder="Search Coin"
              className="searchCountry"
              onChange={(e) => setSearchStr(e.target.value)}
              onKeyDown={(e) => setSearchStr(e.target.value)}
            />
            <Scrollbars
              className="countryListScroll"
              renderThumbHorizontal={() => <div />}
              renderThumbVertical={() => <div />}
              renderView={(props) => <div {...props} className="countryList" />}
            >
              {coinList
                .filter(
                  (coin) =>
                    coin.coinSymbol.includes(searchStr) ||
                    coin.coinName.includes(searchStr)
                )
                .map((coin) => (
                  <div
                    key={coin.coinSymbol}
                    className="listCountry"
                    onClick={() => {
                      setCoin(coin);
                      onClose();
                      
                    }}
                    onKeyDown={() => {
                      setCoin(coin);
                      onClose();
                      
                    }}
                    role="presentation"
                    
                  >
                    <div className="name">
                      {coin.coinName}({coin.coinSymbol})
                    </div>
                    <img src={coin.coinImage} alt="" className="flag" />
                  </div>
                ))}
            </Scrollbars>
          </div>
        </div>
      </div>
    </>
  );
}

export default CoinSelectModal;
