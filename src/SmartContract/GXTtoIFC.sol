// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol";

contract TokenTransfer {
    IERC20 private tokenGXT;
    IERC20 private tokenIFC;
    address private owner;

    constructor(
        address _tokenGXT,
        address _tokenIFC
    ) {
        tokenGXT = IERC20(_tokenGXT);
        tokenIFC = IERC20(_tokenIFC);
        owner=msg.sender;
    }

    //Before executing this function user need to provide allowance to this smart contract
    function Swap(uint _amount) external {
        require(_amount> 0, "Token amount must be greater than zero");
        require(tokenGXT.balanceOf(msg.sender) >= _amount, "Insufficient balance");
        require(tokenGXT.allowance(msg.sender, address(this)) >= _amount, "Insufficient allowance");
        require(tokenIFC.balanceOf(address(this)) >= _amount, "Currently not enough tokens in the contract, please retry later");
        IERC20(tokenGXT).transferFrom(msg.sender, address(this), _amount);
        tokenIFC.transfer(msg.sender, _amount);
    }

    function GXTSC() external view returns(uint) {
        require(msg.sender == owner,"Ownership Assertion: Caller of the function is not the owner.");
        return tokenGXT.balanceOf(address(this));
    }

    //
    function IFCSC() external view returns(uint) {
        require(msg.sender == owner,"Ownership Assertion: Caller of the function is not the owner.");
        return tokenIFC.balanceOf(address(this));
    }
    
    function GXTB() external view returns(uint) {
        return tokenGXT.balanceOf(msg.sender);
    }

    function IFCB() external view returns(uint) {
        return tokenIFC.balanceOf(msg.sender);
    }  

    //GXToken withdrawGXT function
    function withdrawGXT(uint amount) external {
        require(msg.sender == owner,"Ownership Assertion: Caller of the function is not the owner.");
        require(amount> 0, "Token amount must be greater than zero");
        require(tokenGXT.balanceOf(address(this)) >= amount, "Insufficient GXToken's in contract");
        tokenGXT.transfer(msg.sender, amount);
    }

    //IFCToken withdrawIFC function
    function withdrawIFC(uint amount) external {
        require(msg.sender == owner,"Ownership Assertion: Caller of the function is not the owner.");
        require(amount> 0, "Token amount must be greater than zero");
        require(tokenIFC.balanceOf(address(this)) >= amount, "Insufficient IFCToken's in contract");
        tokenIFC.transfer(msg.sender, amount);
    }
}