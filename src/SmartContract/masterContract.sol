//contract from purchase and swapping
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol"; // Import the ERC20 interface

contract TokenSwap {
    address public admin; // Address of the contract admin
    mapping(address => mapping(address => uint256)) public exchangeRates; // Mapping to store exchange rates between tokens
    event TokenSwapEvent(address indexed buyer, address indexed fromToken, address indexed toToken, uint256 fromAmount, uint256 toAmount);

    constructor() {
        admin = msg.sender;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin, "Only admin can call this function");
        _;
    }

    modifier validAddress(address _addr) {
        require(_addr != address(0), "Not a valid address");
        _;
    }

    function changeAdmin(address _newAdmin) public onlyAdmin validAddress(_newAdmin) {
        admin = _newAdmin;
    }

    // Function to set the exchange rate between two tokens with only admin and valid address
    function setExchangeRateToken(address _fromToken, address _toToken, uint256 _rate) external onlyAdmin validAddress(_fromToken) validAddress(_toToken) {
        exchangeRates[_fromToken][_toToken] = _rate;
    }

    // Function to set exchange rate between ETH and a token
    function setExchangeRateEth(address _token, uint256 _rateInWei) external onlyAdmin validAddress(_token) {
        // Require non-zero rate
        require(_rateInWei > 0, "Exchange rate must be greater than 0");

        // Store the exchange rate
        exchangeRates[address(0)][_token] = _rateInWei;
    }

    // Function to get exchange rate between ETH and a token
    function getExchangeRateEth(address _token) external view returns (uint256) {
        uint256 rate = exchangeRates[address(0)][_token];
        require(rate > 0, "Exchange rate not available");
        return rate;
    }

    // Get Estimation of the amount of tokens to be received with only admin and valid address
    function getEstimationOfToken(address _fromToken, address _toToken, uint256 _fromAmount) external view onlyAdmin validAddress(_fromToken) validAddress(_toToken) returns (uint256) {
        uint256 rate = exchangeRates[_fromToken][_toToken];
        require(rate > 0, "Exchange rate not available");
        uint256 toAmount = (_fromAmount / rate)*10**18;
        require(toAmount > 0, "Token amount must be greater than 0");
        return toAmount;
    }

    // Get Estimation of the amount of tokens to be received in exchange for ETH with only admin and valid address
    function getEstimationOfEth(address _toToken, uint256 _fromAmount) external view onlyAdmin validAddress(_toToken) returns (uint256) {
        uint256 rate = exchangeRates[address(0)][_toToken];
        require(rate > 0, "Exchange rate not available");
        uint256 toAmount = (_fromAmount / rate)*10**18;
        require(toAmount > 0, "Token amount must be greater than 0");
        return toAmount;
    }

    // Function to purchase tokens with ETH with valid address
    function purchaseTokenWithETH(address _toToken) payable external validAddress(_toToken) {
        uint256 rate = exchangeRates[address(0)][_toToken];
        require(rate > 0, "Exchange rate not available");
        uint256 ethAmount = msg.value;
        uint256 toAmount = (ethAmount/rate)*10**18;
        IERC20(_toToken).transfer(msg.sender, toAmount);

        emit TokenSwapEvent(msg.sender, address(0), _toToken, ethAmount, toAmount);
    }

    // Receive function to receive Ether transfers
    receive() external payable {
        // Optional logic to handle received Ether, if needed
    }
    // Function to swap between two tokens with valid address
    function swapTokens(address _fromToken, address _toToken, uint256 _fromAmount) external validAddress(_fromToken) validAddress(_toToken) {
        uint256 rate = exchangeRates[_fromToken][_toToken];
        require(rate > 0, "Exchange rate not available");
        uint256 toAmount = (_fromAmount / rate)*10**18;
        IERC20 fromToken = IERC20(_fromToken);
        uint256 fromTokenBalance = fromToken.balanceOf(msg.sender);
        require(fromTokenBalance >= _fromAmount/10**18, "Insufficient balance of the from token");
        fromToken.transferFrom(msg.sender, address(this), _fromAmount);
        IERC20 toToken = IERC20(_toToken);
        uint256 toTokenBalance = toToken.balanceOf(address(this));
        require(toTokenBalance >= toAmount, "Insufficient balance of the to token");
        toToken.transfer(msg.sender, toAmount);
        emit TokenSwapEvent(msg.sender, _fromToken, _toToken, _fromAmount, toAmount);
    }

    //Function to withdraw ETH from the contract with only admin and valid address
    function withdrawETH(uint256 _amount) external onlyAdmin validAddress(msg.sender) {
        require(address(this).balance >= _amount, "Insufficient ETH balance in the contract");
        (bool success, ) = payable(admin).call{value: _amount}("");
        require(success, "ETH transfer failed");
    }

    //Function to add tokens to the contract with only admin and valid address
    function addTokens(address _token, uint256 _amount) external onlyAdmin validAddress(_token) {
        IERC20(_token).transferFrom(msg.sender, address(this), _amount);
    }

    //Function to withdraw tokens from the contract with only admin and valid address
    function withdrawTokens(address _token, uint256 _amount) external onlyAdmin validAddress(_token) {
        require(IERC20(_token).balanceOf(address(this)) >= _amount, "Insufficient token balance in the contract");
        IERC20(_token).transfer(admin, _amount);
    }

    //Function to know the balance of the token in the contract with only admin and valid address
    function getBalance(address _token) external view onlyAdmin validAddress(_token) returns (uint256) {
        return IERC20(_token).balanceOf(address(this));
    }   

    //Function to know the balance of the ETH in the contract with only admin and valid address
    function getETHBalance() external view onlyAdmin returns (uint256) {
        return address(this).balance;
    }
}