import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { ChatsContext } from "../context/ChatsContext";
import virtualProspectus from "../static/images/logos/influenceCoin.svg";

function Navbar({ invert, className }) {
  const { chatOn, setChatOn } = useContext(ChatsContext);
  const history = useHistory();
  return (
    <nav className={`navbar ${className} ${invert}`}>
      <div className="navBtn" onClick={() => history.push("/converter")}>
        Converter
      </div>
      <img
        src={virtualProspectus}
        alt="cryptobanklogo"
        className="crypto-img"
        onClick={() => history.push("/")}
      />
      <div className="navBtn" onClick={() => setChatOn(!chatOn)}>
        Chats
      </div>
    </nav>
  );
}

export default Navbar;
