import GXTabi from "./SmartContract/abi/GXT.json"
import IFCabi from "./SmartContract/abi/IFC.json"
import Purchaseabi from "./SmartContract/abi/ethSwappingContractAbi.json"
import Swapabi from "./SmartContract/abi/GXTtoIFCAbi.json"

const GXTToken = {
  address: "0xf1bc345c29b683f2856c03b71332829f1ae897ae",
  tokenName: "GX Token",
  tokenSymbol: "GXT",
  tokenDecimals: 18,
  totalSupply: "350000000000000000000000000",
  abi: GXTabi,
};

const IFCToken = {
  address: "0x446b22b6eb2d1c54fe0172a46e58793f8c2bc633",
  tokenName: "IF Coin",
  tokenSymbol: "IFC",
  tokenDecimals: 18,
  totalSupply: "350000000000000000000000000",
  abi: IFCabi,
};

const PurchaseSmartContract = {
  address: "0xb4c5b6eae00672e43d8c01fd83b73f1671b56016",
  abi: Purchaseabi,
};

const SwapSmartContract = {
  address: "0xac38c65c2f0b51dc99ad328478c1bff9cb6c9e80",
  abi: Swapabi,
};

export { GXTToken, IFCToken, PurchaseSmartContract, SwapSmartContract };
