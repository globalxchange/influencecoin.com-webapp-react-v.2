// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol";

contract TokenTransfer {
    IERC20 private tokenIFC;
    uint256 public ratio=400000000000000;
    address public owner;

    constructor(
        address _tokenIFC
    ) {
        tokenIFC = IERC20(_tokenIFC);
        owner=msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Not owner");
        _;
    }

    modifier validAddress(address _addr) {
        require(_addr != address(0), "Not valid address");
        _;
    }

    function changeOwner(address _newOwner) public onlyOwner validAddress(_newOwner) {
        owner = _newOwner;
    }

    receive() external payable{
        uint256 reminder=msg.value%ratio;
        require(reminder==0,"Incorrect Multiple");
        uint256 TokenBalance=IERC20(tokenIFC).balanceOf(address(this));
        uint256 amount=msg.value/ratio;
        require(TokenBalance>=amount*10**18,"Currently not enough IFC in the contract, please try again later");
        tokenIFC.transfer(msg.sender, amount*10**18);
    }

    function withdrawETH() public onlyOwner {
        address payable to = payable(owner);
        to.transfer(getETHBalance());
    }

    function getETHBalance() public onlyOwner view returns (uint256) {
        return address(this).balance;
    }

    function ContractTokenBalance(address _tokenContract) external view onlyOwner validAddress(_tokenContract) returns(uint256) {
        return IERC20(_tokenContract).balanceOf(address(this));
    }

    function UserTokenBalance(address _tokenContract) external view validAddress(_tokenContract) returns(uint256) {
        return IERC20(_tokenContract).balanceOf(msg.sender);
    }

    function setRatio(uint256 _ratio) public onlyOwner {
        ratio = _ratio;
    }

    function getRatio() public view onlyOwner returns(uint256){
        return ratio;
    }

    function getEstimation(uint256 _amount) public view returns(uint256){
        uint256 reminder=_amount%ratio;
        require(reminder==0,"Incorrect Multiple");
        uint256 estimation=(_amount*(10**18))/ratio;
        return estimation;
    }

    function withdrawToken(address _tokenContract, uint256 _amount) external onlyOwner validAddress(_tokenContract){
        IERC20(_tokenContract).transfer(owner, _amount);
    }
}